class AnswersController < ApplicationController

	def index
		if session[:user_id]
			uri = URI('http://localhost:3000/answers')
			answers_params = {answer_id: params["id"], user_id: session[:user_id]  }
			uri.query = URI.encode_www_form(answers_params)
			@answer_res = Net::HTTP.get_response(uri)

			if @answer_res.code == "200"
				@answer_res = JSON.parse(@answer_res.body)
				render 'answers/answer'
			else
				@error_message = "Record does not exist !! "
				render 'errors/error'
			end

		else
			flash[:notice] = "Please login to view this page"
			redirect_to controller: 'sessions', action: 'new'
		end
	end

	def create
		if params["answer"].empty?
			flash[:alert] = "Please Enter Valid Answer !!"
			redirect_to controller: 'questions', action: 'index', id: params["id"]
		else
			uri = URI('http://localhost:3000/answers')
			answers_params = {answer: params[:answer], question_id: params["id"], user_id: session[:user_id]  }
			res = Net::HTTP.post_form(uri, answers_params)
			@posted_answer = JSON.parse(res.body)
			# redirect_to controller: 'questions', action: 'search'
			redirect_to controller: 'questions', action: 'index', id: params["id"]
			# render 'questions/show'
		end
	end

	def destroy
		uri = URI('http://localhost:3000/answers/1')
		http = Net::HTTP.new(uri.host, uri.port)
		attribute_url = '?'
		body = { user_id: session["user_id"] , answer_id: params[:id] }
		attribute_url << body.map{|k,v| "#{k}=#{v}"}.join('&')
    request = Net::HTTP::Delete.new(uri.request_uri + attribute_url)
		response = http.request(request)
		
		redirect_to controller: 'users', action: 'index'
	end


	def edit
		if session[:user_id]
			uri = URI('http://localhost:3000/answers')
			answer_params = {answer_id: params["id"], user_id: session[:user_id]  }
			uri.query = URI.encode_www_form(answer_params)
			@answer_res = Net::HTTP.get_response(uri)

			if @answer_res.code == "200"
				@answer_res = JSON.parse(@answer_res.body)
			else
				@error_message = "Record does not exist !! "
			end
		else
			flash[:notice] = "Please login to view this page"
			redirect_to controller: 'sessions', action: 'new'
		end		
	end
	
	def update
		if session[:user_id]
			uri = URI('http://localhost:3000/answers/1')
			req = Net::HTTP::Put.new(uri)
			req.set_form_data('user_id' => session[:user_id], 'answer_id' => params[:id], "answer": params[:value] )
			res = Net::HTTP.start(uri.hostname, uri.port) do |http|
				http.request(req)
			end

			if res.code == "200"
				res = JSON.parse(res.body)
				redirect_to controller: 'answers', action: 'index', id: res["id"]
			else
				@error_message = "Record does not exist !! "
				render 'errors/error'
			end
		else
			flash[:notice] = "Please login to view this page"
			redirect_to controller: 'sessions', action: 'new'
		end
	end



end