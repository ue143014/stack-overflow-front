class VotesController < ApplicationController

  def create
    uri = URI('http://localhost:3000/votes')
    vote_params = {user_id: session[:user_id], voteable_type: params[:type], voteable_id: params[:id], vote: params[:weight] }
    res = Net::HTTP.post_form(uri, vote_params)

    if res.code == "200" && res.body != "null"

      @posted_vote = JSON.parse(res.body)

      if params[:type] == "Question"
        redirect_to controller: 'questions', action: 'index', id: params["id"]
      else
        redirect_to controller: 'answers', action: 'index', id: params["id"]
      end
    
    else
      if params[:type] == "Question"
        flash[:notice] = "You cannot vote own question"
        redirect_to controller: 'questions', action: 'index', id: params["id"]
      else
        flash[:notice] = "You cannot vote own answer"
        redirect_to controller: 'answers', action: 'index', id: params["id"]
      end
    end
    # render 'questions/show'
  end

end