class UsersController < ApplicationController

	def new
		@user
	end

	def index

		if !(session["user_id"])
			user_params = { user_id: params["id"] }
		else
			user_params = {user_id: session["user_id"]}
		end

		uri = URI('http://localhost:3000/users')
		
		uri.query = URI.encode_www_form(user_params)
		@user = Net::HTTP.get_response(uri)

		if @user.code == "200"
			@user = @user.body
			@user = JSON.parse(@user)

			uri = URI('http://localhost:3000/tags')
			tag_params = {tag: '', user_id: @user["id"] }
			uri.query = URI.encode_www_form(tag_params)
			tag_res = Net::HTTP.get_response(uri)
			@tag = tag_res.body			
			@tag = JSON.parse(@tag)

			render 'users/show'
		else
			flash[:notice] = "Please login to Post an Answer !! "
			redirect_to controller: 'sessions', action: 'new'
		end
	end

	def create
		if params[:name].empty? || params[:email].empty? || params[:password].empty?
			flash[:alert] = "Please provide valid values"
			redirect_to controller: 'users', action: 'new'
		else
			uri = URI('http://localhost:3000/users')
			user_params = {name: params[:name], email: params[:email], password: params[:password]  }
			res = Net::HTTP.post_form(uri, user_params)

			if res.code == "200"
				@user = res.body
				@user = JSON.parse(@user)
				log_in @user
				redirect_to controller: 'users', action: 'index', id: @user["id"]
			else
				flash[:alert] = "Email already Exists"
				redirect_to controller: 'users', action: 'new'
			end
			
		end
		# render 'static_pages/home'
  end

end