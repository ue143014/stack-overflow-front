class QuestionsController < ApplicationController

	def index
		if session[:user_id]
			uri = URI('http://localhost:3000/questions')
			question_params = {question_id: params["id"], user_id: session[:user_id]  }
			uri.query = URI.encode_www_form(question_params)
			@question_res = Net::HTTP.get_response(uri)

			if @question_res.code == "200"
				@question_res = JSON.parse(@question_res.body)
				render 'questions/question'
			else
				@error_message = "Record does not exist !! "
				render 'errors/error'
			end
		else
			flash[:notice] = "Please login to view this page"
			redirect_to controller: 'sessions', action: 'new'
		end
	end

	def new
		uri = URI('http://localhost:3000/tags')
		tag_params = {tag: '' }
		uri.query = URI.encode_www_form(tag_params)
		tag_res = Net::HTTP.get_response(uri)
		@tag = tag_res.body
		@tag = JSON.parse(@tag)
	end

	def create
		if params["question"].empty?
			flash[:alert] ="Please enter valid Question !! "
			redirect_to controller: 'users', action: 'index'
		else
			uri = URI('http://localhost:3000/questions')
			tag_params = {question: params[:question], "tags[]": params[:tags].split(","), user_id: session[:user_id]  }
			res = Net::HTTP.post_form(uri, tag_params)
			@posted_question = JSON.parse(res.body)
			redirect_to controller: 'questions', action: 'index', id: @posted_question["id"]
			# render 'users/show'
		end
	end

	def search

		if params[:tags] != ""
			uri = URI('http://localhost:3000/search')
			tag_list = params[:tags].split(",")
			res = Net::HTTP.post_form(uri, "tags[]" => tag_list)
			@search_data = res.body
			@search_data = JSON.parse(@search_data)
			@search_data = @search_data.paginate(:page => 1, :per_page => 10)
			render 'show'
		else
			flash[:alert] = "Please Enter Tags"
			redirect_to controller: 'questions', action: 'new'
		end
	end

	def destroy
		uri = URI('http://localhost:3000/questions/1')
		http = Net::HTTP.new(uri.host, uri.port)
		attribute_url = '?'
		body = { user_id: session["user_id"] , question_id: params[:id] }
		attribute_url << body.map{|k,v| "#{k}=#{v}"}.join('&')
    request = Net::HTTP::Delete.new(uri.request_uri + attribute_url)
		response = http.request(request)
		
		redirect_to controller: 'users', action: 'index'
	end


	def edit
		if session[:user_id]
			uri = URI('http://localhost:3000/questions')
			question_params = {question_id: params["id"], user_id: session[:user_id]  }
			uri.query = URI.encode_www_form(question_params)
			@question_res = Net::HTTP.get_response(uri)

			if @question_res.code == "200"
				@question_res = JSON.parse(@question_res.body)
			else
				@error_message = "Record does not exist !! "
			end
		else
			flash[:notice] = "Please login to view this page"
			redirect_to controller: 'sessions', action: 'new'
		end		
	end
	
	def update
		if session[:user_id]
			uri = URI('http://localhost:3000/questions/1')
			req = Net::HTTP::Put.new(uri)
			req.set_form_data('user_id' => session[:user_id], 'question_id' => params[:id], "question": params[:value] )
			res = Net::HTTP.start(uri.hostname, uri.port) do |http|
				http.request(req)
			end

			if res.code == "200"
				res = JSON.parse(res.body)
				redirect_to controller: 'questions', action: 'index', id: res["id"]
			else
				@error_message = "Record does not exist !! "
				render 'errors/error'
			end
		else
			flash[:notice] = "Please login to view this page"
			redirect_to controller: 'sessions', action: 'new'
		end
	end

end